from flask import Response, abort
from flask_restful import reqparse, Resource, inputs

from hmapi.app import api, mongo
import traceback

from hmapi.auth import requires_auth

user_parser = reqparse.RequestParser()
user_parser.add_argument('firstname', type=str, required=True, help="The user's firstname")
user_parser.add_argument('lastname', type=str, required=True, help="The user's lastname")
user_parser.add_argument('nickname', type=str, required=True, help="The user's nickname")
user_parser.add_argument('gender', type=str, required=True, help="The user's gender")
user_parser.add_argument('email', type=inputs.regex('\\b[A-Za-z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}\\b'),
                         required=True, help="The user's email")
user_parser.add_argument('credentials', type=str, required=True, help="The user's credentials")
user_parser.add_argument('birthdate', type=inputs.date, required=True,
                         help="The user's birthdate")
user_parser.add_argument('picture', type=inputs.url, help="The user's image URL")
user_parser.add_argument('languages', type=str, help="The user's spoken languages", action='append')
user_parser.add_argument('volunteering', type=inputs.boolean, help="If the user is volunteering")
user_parser.add_argument('location', type=float, help="User's current location", action='append')
user_parser.add_argument('address', type=unicode, help="User's home address")
user_parser.add_argument('twitter', type=str, help="User's twitter handle")
user_parser.add_argument('facebook', type=str, help="User's facebook url")


class Users(Resource):
    def get(self):
        get_parser = reqparse.RequestParser()
        get_parser.add_argument('volunteers', type=inputs.boolean, help="Get users type")
        args = get_parser.parse_args()
        volunteering = args.get('volunteers', False)

        users_list = mongo.db.users.find({'volunteering': volunteering})
        users = []
        for u in users_list:
            u['_id'] = str(u['_id'])
            u['birthdate'] = u['birthdate'].date().isoformat()
            users.append(u)
        return users

    @requires_auth
    def post(self):
        args = user_parser.parse_args(strict=True)
        user = {}
        for k, v in args.iteritems():
            if v:
                user[k] = v
        u = mongo.db.users.insert_one(user)
        if u:
            url = api.url_for(User, user_id=u.inserted_id)
            return Response(status=201, headers={'Location': url})
        else:
            abort(500)


class User(Resource):
    def get(self, user_id):
        user = mongo.db.users.find_one_or_404({'_id': user_id})
        try:
            user['_id'] = str(user['_id'])
            user['birthdate'] = user['birthdate'].date().isoformat()
            return user
        except:
            traceback.print_exc()
            raise

    @requires_auth
    def put(self, user_id):
        update_parser = reqparse.RequestParser()        
        update_parser.add_argument('firstname', type=str, help="The user's firstname")
        update_parser.add_argument('lastname', type=str, help="The user's lastname")
        update_parser.add_argument('nickname', type=str, help="The user's nickname")
        update_parser.add_argument('gender', type=str, help="The user's gender")
        update_parser.add_argument('email',
                                     type=inputs.regex('\\b[A-Za-z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}\\b'),
                                     help="The user's email")
        update_parser.add_argument('credentials', type=str, help="The user's credentials")
        update_parser.add_argument('birthdate', type=inputs.date, help="The user's birthdate")

        update_parser.add_argument('picture', type=inputs.url, help="The user's image URL")
        update_parser.add_argument('languages', type=str, help="The user's spoken languages", action='append')
        update_parser.add_argument('volunteering', type=inputs.boolean, help="If the user is volunteering")
        update_parser.add_argument('location', type=float, help="User's current location", action='append')
        update_parser.add_argument('address', type=unicode, help="User's home address")
        update_parser.add_argument('twitter', type=str, help="User's twitter handle")
        update_parser.add_argument('facebook', type=str, help="User's facebook url")

        args = update_parser.parse_args()
        mod_user = mongo.db.users.find_one_or_404({'_id': user_id})
        for k, v in args.iteritems():
            if v:
                mod_user[k] = v
        mongo.db.users.replace_one({'_id': user_id}, mod_user)
        return Response(status=204)
