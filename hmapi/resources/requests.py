from flask import Response, abort, request
from flask_restful import reqparse, Resource, inputs
from flask_pymongo import ObjectId
import traceback
from hmapi.app import api, mongo
from datetime import datetime

from hmapi.auth import requires_auth

def status_str(param):
    allowed = ['Pending', 'Assigned', 'Arranged', 'Done']
    if param not in allowed:
        raise ValueError('Provided status not allowed')


request_parser = reqparse.RequestParser()
request_parser.add_argument('initiated_by', type=str, required=True, help="Request's initiator id")
request_parser.add_argument('initiated_on', type=inputs.datetime_from_iso8601, required=True, help="Request's date")
request_parser.add_argument('description', type=unicode, required=True, help="Request's details")
request_parser.add_argument('scheduled_for', type=inputs.datetime_from_iso8601, help="Request's scheduled date")
request_parser.add_argument('place', type=unicode, help="Request's place requested/arranged")
request_parser.add_argument('status', type=status_str, help="Request's status (Pending, Arranged or Done)")

assignee_parser = reqparse.RequestParser()
assignee_parser.add_argument('assigned_to', type=str, required=True, help="Assignee id")

comment_parser = reqparse.RequestParser()
comment_parser.add_argument('posted_by', type=str, required=True, help="Comment's user id")
comment_parser.add_argument('text', type=unicode, required=True, help="Comment's text")

class Requests(Resource):
    def get(self):
        get_parser = reqparse.RequestParser()
        get_parser.add_argument('user', type=str, help="Query by user")
        get_parser.add_argument('status', type=str, help="Query by status", action='append')
        args = get_parser.parse_args()

        assigned_user = args.get('user')
        status = args.get('status')
        qry = {}
        if assigned_user:
            qry['assigned_to'] = assigned_user

        if status and len(status)>0:
            qry['status'] = {'$in': status}

        requests_list = mongo.db.requests.find(qry)
        requests = []
        for r in requests_list:
            r['_id'] = str(r['_id'])
            r['initiated_on'] = r['initiated_on'].isoformat()
            if r.get('assigned_on'):
                r['assigned_on'] = r['assigned_on'].isoformat()
            if r.get('scheduled_for'):
                r['scheduled_for'] = r['scheduled_for'].isoformat()
            requests.append(r)
        return requests

    @requires_auth
    def post(self):
        args = request_parser.parse_args()
        req = {}
        for k, v in args.iteritems():
            if v:
                req[k] = v
        if not req.get('status'):
            req['status'] = 'Pending'

        validate_user = mongo.db.users.find_one({'_id':ObjectId(req['initiated_by'])})
        if validate_user is None:
            abort(400)

        r = mongo.db.requests.insert_one(req)
        if r:
            url = api.url_for(Request, request_id=r.inserted_id)
            return Response(status=201, headers={'Location': url})
        else:
            abort(500)

class Request(Resource):
    def get(self, request_id):
        req_db = mongo.db.requests.find_one_or_404({'_id': request_id})
        try:
            req_db['_id'] = str(req_db['_id'])
            req_db['initiated_on'] = req_db['initiated_on'].isoformat()
            if req_db.get('assigned_on'):
                req_db['assigned_on'] = req_db['assigned_on'].isoformat()
            if req_db.get('scheduled_for'):
                req_db['scheduled_for'] = req_db['scheduled_for'].isoformat()
            return req_db
        except:
            traceback.print_exc()
            raise

    @requires_auth
    def put(self, request_id):
        req_db = mongo.db.requests.find_one_or_404({'_id': request_id})
        update_parser = reqparse.RequestParser()
        update_parser.add_argument('description', type=unicode, help="Request's details")
        update_parser.add_argument('scheduled_for', type=inputs.datetime_from_iso8601, help="Request's scheduled date")
        update_parser.add_argument('place', type=unicode, help="Request's place requested/arranged")
        update_parser.add_argument('status', type=status_str, help="Request's status (Pending, Arranged or Done)")
        args = update_parser.parse_args(strict=True)

        for k, v in args.iteritems():
            if v:
                req_db[k] = v
        mongo.db.requests.replace_one({'_id': request_id}, req_db)
        return Response(status=204)

class RequestAssignee(Resource):
    def get(self, request_id):
        r = mongo.db.requests.find_one_or_404({'_id': request_id})
        if r.get('assigned_to') and r.get('assigned_on'):
            ret = {
                'assigned_to': str(r['assigned_to']),
                'assigned_on': r['assigned_on'].isoformat()
            }
            return ret
        else:
            abort(404)

    @requires_auth
    def put(self, request_id):
        mongo.db.requests.find_one_or_404({'_id': request_id})
        args = assignee_parser.parse_args()
        fields = {
            'assigned_to': args.get('assigned_to'),
            'assigned_on': datetime.now(),
            'status': 'Assigned'
        }

        validate_user = mongo.db.users.find_one({'_id':ObjectId(fields['assigned_to'])})
        if validate_user is None:
            abort(400)

        mongo.db.requests.update_one({'_id': request_id}, {'$set': fields})
        return Response(status=204)

class RequestComments(Resource):
    def get(self, request_id):
        r = mongo.db.requests.find_one_or_404({'_id': request_id})
        if not r.get('comments'):
            abort(404)

        return r['comments']

    @requires_auth
    def post(self, request_id):
        mongo.db.requests.find_one_or_404({'_id': request_id})
        args = comment_parser.parse_args()
        cmt_id = ObjectId()
        cmt = {
            '_id': cmt_id,
            'posted_by': args['posted_by'],
            'posted_on': datetime.now(),
            'text': args['text'],
        }

        validate_user = mongo.db.users.find_one({'_id':ObjectId(cmt['posted_by'])})
        if validate_user is None:
            abort(400)

        modified = mongo.db.requests.update_one({'_id': request_id},
                                                  {'$push': {'comments': cmt}})
        if modified.modified_count:
            url = api.url_for(RequestComment, request_id=request_id, comment_id = str(cmt_id))
            return Response(status=201, headers={'Location': url})
        else:
            abort(500)

class RequestComment(Resource):
    def get(self, request_id, comment_id):
        req = mongo.db.requests.find_one_or_404({'_id': request_id})
        if isinstance(req['comments'], list):
            c = next(cmt for cmt in req['comments'] if cmt['_id']==comment_id)
            c['_id'] = str(c['_id'])
            c['posted_on'] = c['posted_on'].isoformat()
            return c

        abort(404)

    @requires_auth
    def delete(self, request_id, comment_id):
        r = mongo.db.requests.find_one_or_404({'_id': request_id})
        mod_data = mongo.db.requests.update_one({},
                                     {'$pull': {'comments': {'_id':comment_id }}})
        if mod_data.modified_count:
            return Response(status=204)
        else:
            return Response(status=404)

