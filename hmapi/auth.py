from functools import wraps
from flask import request, Response
from werkzeug.security import check_password_hash


def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    return (check_password_hash('pbkdf2:sha1:1000$PcsAfQrE$3cb7ee50fcef6fbb09a3c7751c4742b07453f071', username) and
            check_password_hash('pbkdf2:sha1:1000$EuJdhT8U$de78f909b8490d99a74540271b3cd1fc9ab6ee75', password))

def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
    '{"message":"Could not verify your access level for that URL. '
    'You have to login with proper credentials"}', 401,
    {'WWW-Authenticate': 'Basic realm="Login Required"'})

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        from hmapi.app import app
        auth = request.authorization
        if not app.testing and (not auth or not check_auth(auth.username, auth.password)):
            return authenticate()
        return f(*args, **kwargs)
    return decorated