from flask import Flask
from flask.ext.pymongo import PyMongo
from pymongo import GEO2D
from flask_sslify import SSLify

app = Flask(__name__)
app.config.from_object('config')

mongo = PyMongo(app)

sslify = SSLify(app, permanent=True)

@app.before_request
def ensure_index():
    mongo.db.users.create_index([("location", GEO2D)])

# Flask-RESTful
from flask_restful import Api
from flask.ext.restful.representations.json import output_json
#output_json.func_globals['settings'] = {'ensure_ascii': False, 'encoding': 'utf8'}

api = Api(app)

from hmapi.resources.users import Users, User
from hmapi.resources.requests import Requests, Request, RequestComments, RequestComment, RequestAssignee

api.add_resource(Users, '/users', endpoint='users')
api.add_resource(User, '/users/<ObjectId:user_id>', endpoint='user')

api.add_resource(Requests, '/requests', endpoint='requests')
api.add_resource(Request, '/requests/<ObjectId:request_id>', endpoint='request')
api.add_resource(RequestAssignee, '/requests/<ObjectId:request_id>/assignee', endpoint='request_assignee')
api.add_resource(RequestComments, '/requests/<ObjectId:request_id>/comments', endpoint='request_comments')
api.add_resource(RequestComment, '/requests/<ObjectId:request_id>/comments/<ObjectId:comment_id>', endpoint='request_comment')

