# Helping Moon - RESTfull API

Helping Moon volunteer API

## Demo REST API

Deployed at [https://helping-moon-dev.herokuapp.com](http://helping-moon-dev.herokuapp.com)

## Simple database

### User
* **firstname**
* **lastname**
* **nickname**
* **gender**
* **email**
* **credentials**
* **birthdate**: e.g. 1990-01-01
* picture
* languages: ['english', 'finnish']
* volunteering: 'true'|'false'
* location: [longitude, latitude] e.g. in Oulu [25.497468, 65.052352]
* address
* twitter
* facebook 

\* **mandoatory** fields when creating new user

### Request
* initiated_by
* initiated_on
* description
* scheduled_for
* place
* status: Pending, Assigned, Arranged, Done
* assigned_to
* assigned_on
* comments: [ **posted_by**, **text** ]


## Resources

| URL 
|---------------
| `/api/users`
| `/api/users/<user_id>`
| `/api/requests` 
| `/api/requests/<request_id>` 
| `/api/requests/<request_id>/comments`  
| `/api/requests/<request_id>/assignee` 
