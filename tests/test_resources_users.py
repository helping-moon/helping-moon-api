# coding=utf-8
import unittest
from flask import request
from flask_restful import reqparse, Resource, inputs
import datetime
from hmapi.app import app, mongo
import ujson
from copy import copy

class UsersResourceTestCase(unittest.TestCase):
    user1 = {
        'firstname': 'Macho',
        'lastname': 'Fantastico',
        'nickname': 'FantasticO',
        'gender': "male",
        'email': 'fantastic@males.com',
        'credentials': 'top_secret_pass_not_easy_to_guess_BY_machines_BUT_memorable_by_humans',
        'birthdate': datetime.datetime(1990,1,1),
        'picture': "http://www.wpclipart.com/signs_symbol/icons_oversized/male_user_icon.png",
        'languages': ['english','finnish'],
        'volunteering': 'true',
        'location': [25.497468, 65.052352],
        'address': u"On the lake! Järvilla",
        'twitter': "@machooo",
        'facebook': "fb.co/machoooo"
    }

    @classmethod
    def setUpClass(cls):
        app.testing = True
        app.debug = True
        if 'TEST_MONGO' not in app.extensions['pymongo']:
            mongo.init_app(app, 'TEST_MONGO')

    @classmethod
    def tearDownClass(cls):
        with app.test_request_context():
            cl = mongo.db.client
            cl.drop_database(mongo.db)

    def setUp(self):
        with app.test_request_context():
            user_copy = copy(self.user1)
            u = mongo.db.users.insert_one(user_copy)
            assert u is not None
            self.user_id1= str(u.inserted_id)


    def tearDown(self):
        with app.test_request_context():
            mongo.db.users.drop()

    def test_mongo_init(self):
        with app.test_request_context():
            self.assertEqual(mongo.db.name, 'helpingmoon_test')

    def test_reqparser(self):
        with app.test_request_context('/', content_type='application/json', data='{"firstname":"aaa", "b":{"c":"d"}, "extra":"something"}') as c:
            post_parser = reqparse.RequestParser()
            post_parser.add_argument('firstname', type=str, required=True, help="The user's firstname")
            args = post_parser.parse_args()
            self.assertEqual(args.get('firstname'), 'aaa')
            self.assertEqual(request.json.get('extra'), 'something')
            self.assertIsInstance(request.json.get('b'), dict)

    def test_post_user(self):
        with app.test_client() as c:
            user_copy = copy(self.user1)
            user_copy['nickname'] = "Fantastico2"
            user_copy['birthdate'] = "1990-02-01"
            r = c.post('/users', content_type='application/json', data = ujson.dumps(user_copy))
            assert r.status_code == 201
            u = mongo.db.users.find_one({'nickname': 'Fantastico2'})
            assert u is not None
            assert isinstance(u.get('birthdate'),  datetime.datetime)
            assert u.get('birthdate').date().isoformat() == '1990-02-01'
            assert isinstance(u.get('location'), list)
            assert u['location'] == [25.497468, 65.052352]
            assert isinstance(u.get('languages'), list)
            assert u['languages'] == ['english','finnish']

    def test_get_user(self):
        with app.test_client() as c:
            r2 = c.get('/users/' + self.user_id1)
            assert r2.status_code == 200
            d = ujson.loads(r2.data)
            assert isinstance(d, dict)
            assert d['birthdate'] == '1990-01-01'
            assert d['location'] == [25.497468, 65.052352]

    def test_update_user(self):
        with app.test_client() as c:
            r2 = c.put('/users/' + self.user_id1, content_type='application/json',
                       data='{"location": [25.4976, 65.0525]}')
            assert r2.status_code == 204
            u = mongo.db.users.find_one({'nickname': 'FantasticO'})
            assert u['location'] == [25.4976, 65.0525]


if __name__ == '__main__':
    unittest.main()
