# coding=utf-8
import unittest

from flask import request
from flask_restful import reqparse, Resource, inputs
import dateutil.parser
import datetime
from hmapi.app import app, mongo
import ujson
from copy import copy

class RequestsResourceTestCase(unittest.TestCase):
    citizen = {
        'firstname': 'Magnificent',
        'lastname': 'Elisa',
        'nickname': 'magnificent',
        'gender': "female",
        'email': 'magnificent@elisa.com',
        'credentials': 'not_a_secret',
        'birthdate': datetime.datetime(1940,1,1),
        'picture': "http://images.clipartof.com/thumbnails/1166240-Cartoon-Of-A-Granny-Woman-Avatar-On-Pink-Royalty-Free-Vector-Clipart.jpg",
        'languages': ['finnish'],
        'location': [25.558630, 65.066930],
        'address': u"Kalikkalampi"
    }

    volunteer = {
        'firstname': 'Macho',
        'lastname': 'Fantastico',
        'nickname': 'FantasticO',
        'gender': "male",
        'email': 'fantastic@males.com',
        'credentials': 'top_secret_pass_not_easy_to_guess_BY_machines_BUT_memorable_by_humans',
        'birthdate': datetime.datetime(1990,1,1),
        'picture': "http://www.wpclipart.com/signs_symbol/icons_oversized/male_user_icon.png",
        'languages': ['english','finnish'],
        'volunteering': 'true',
        'location': [25.497468, 65.052352],
        'address': u"On the lake! Järvilla",
        'twitter': "@machooo",
        'facebook': "fb.co/machoooo"
    }

    request1 = {
        'initiated_by': '', # will be citizen id
        'initiated_on': datetime.datetime(2015, 8, 5),
        'description': 'please bring me some strawberries',
        'scheduled_for': datetime.datetime(2015, 8, 8),
        'place': 'Kalikkalampi',
        'status': 'Pending'
    }

    # alternating volunteer, citizen:
    comments = [
        'Paljonko mansikkaa haluat?',
        'Paljonko mansikka maksaa tanaan?',
        'Kiloa maksaa lähimain 25 euroa',
        'Ok, haluaisin 100 kiloaa',
        'No neen! Nahaan Kalikkalamppilla!'
    ]

    @classmethod
    def setUpClass(cls):
        app.testing = True
        app.debug = True
        if 'TEST_MONGO' not in app.extensions['pymongo']:
            mongo.init_app(app, 'TEST_MONGO')

    @classmethod
    def tearDownClass(cls):
        with app.test_request_context():
            cl = mongo.db.client
            cl.drop_database(mongo.db)

    def setUp(self):
        self.today_iso = datetime.datetime.now().date().isoformat()
        with app.test_request_context():
            volunteer_copy = copy(self.volunteer)
            citizen_copy = copy(self.citizen)
            u_v = mongo.db.users.insert_one(volunteer_copy)
            assert u_v is not None
            self.volunteer_oid = u_v.inserted_id
            self.volunteer_id = str(u_v.inserted_id)
            u_c = mongo.db.users.insert_one(citizen_copy)
            assert u_c is not None
            self.citizen_oid = u_c.inserted_id
            self.citizen_id = str(u_c.inserted_id)

            request1_copy = copy(self.request1)
            request1_copy ['initiated_by'] = self.citizen_id
            r_db = mongo.db.requests.insert_one(request1_copy)
            self.request1_oid = r_db.inserted_id
            self.request1_id = str(r_db.inserted_id)

    def tearDown(self):
        with app.test_request_context():
            mongo.db.requests.drop()
            mongo.db.users.drop()

    def test_get_requests(self):
        with app.test_client() as c:
            res = c.get('/requests')
            assert res.status_code == 200

            assert res.data is not None
            d_json = ujson.loads(res.data)
            assert isinstance(d_json, list)
            assert len(d_json) == 1
            r0 = d_json[0]

            assert r0.get('initiated_by') == self.citizen_id
            assert 'strawberries' in r0.get('description')
            assert r0.get('initiated_on') == '2015-08-05T00:00:00+00:00'
            assert r0.get('status') == 'Pending'

    def test_post_requests(self):
        request2 = {
            'initiated_by': self.volunteer_id,
            'initiated_on': '2015-08-07T00:00:00+00:00',
            'description': 'I am a volunteer who needs more help to carry 100kg of strawberries!',
            'scheduled_for':'2015-08-08T00:00:00+00:00',
            'place': 'Kalikkalampi'
        }
        with app.test_client() as c:
            data = ujson.dumps(request2)
            res = c.post('/requests', content_type='application/json', data = data )
            assert res.status_code == 201
            r0 = mongo.db.requests.find_one({'initiated_by':self.volunteer_id})
            assert r0 is not None

            assert r0.get('initiated_by') == self.volunteer_id
            assert 'strawberries' in r0.get('description')
            assert r0.get('initiated_on').date().isoformat() == '2015-08-07'
            assert r0.get('status') == 'Pending'

    def test_get_request(self):
        with app.test_client() as c:
            res = c.get('/requests/' + self.request1_id)
            assert res.status_code == 200

            d = res.data
            assert res.data is not None
            d_json = ujson.loads(d)
            assert isinstance(d_json, dict)

            assert d_json.get('initiated_by') == self.citizen_id
            assert 'strawberries' in d_json.get('description')
            assert d_json.get('initiated_on') == '2015-08-05T00:00:00+00:00'
            assert d_json.get('status') == 'Pending'

    def test_put_request(self):
        request_mod = {'description': "Haluaisin mansikka!"}
        with app.test_client() as c:
            res = c.put('/requests/' + self.request1_id, content_type="application/json",
                        data = ujson.dumps(request_mod))
            assert res.status_code == 204
            r_mod = mongo.db.requests.find_one({'_id':self.request1_oid})
            assert r_mod is not None

            assert r_mod.get('initiated_by') == self.citizen_id
            assert 'mansikka' in r_mod.get('description')
            assert r_mod.get('initiated_on').date().isoformat() == '2015-08-05'
            assert r_mod.get('status') == 'Pending'
            assert r_mod.get('assigned_to') is None
            assert r_mod.get('assigned_on') is None

    def test_put_request_assignee(self):
        request_copy = copy(self.request1)
        request_copy['assigned_to'] = self.volunteer_id
        with app.test_client() as c:
            res = c.put('/requests/' + self.request1_id + '/assignee', content_type="application/json",
                        data = ujson.dumps(request_copy))
            assert res.status_code == 204
            r_new = mongo.db.requests.find_one({'_id':self.request1_oid})
            assert r_new is not None

            assert r_new.get('initiated_by') == self.citizen_id
            assert 'strawberries' in r_new.get('description')
            assert r_new.get('initiated_on').date().isoformat() == '2015-08-05'
            assert r_new.get('status') == 'Assigned'
            assert r_new.get('assigned_to') == self.volunteer_id
            assert isinstance(r_new.get('assigned_on'), datetime.datetime)
            # assert r_new.get('assigned_on').date().isoformat() == '2015-08-05'

    def test_get_request_assignee(self):
        request_copy = copy(self.request1)
        request_copy['assigned_to'] = self.volunteer_id
        with app.test_client() as c:
            res = c.put('/requests/' + self.request1_id + '/assignee', content_type="application/json",
                        data = ujson.dumps(request_copy))
            assert res.status_code == 204

            res_get = c.get('/requests/' + self.request1_id + '/assignee')
            assert res_get.status_code == 200

            assert res_get.data is not None
            d_json = ujson.loads(res_get.data)
            assert isinstance(d_json, dict)

            assert d_json.get('assigned_to') == self.volunteer_id
            assert isinstance(dateutil.parser.parse(d_json.get('assigned_on')), datetime.datetime)

    def test_get_request_comments(self):
        alternate = 0
        cmts = []
        for c in self.comments:
            cmt = {
                'posted_by': self.volunteer_id if alternate == 0 else self.citizen_id,
                'text': c
            }
            alternate = not alternate
            cmts.append(cmt)

        with app.test_request_context():
            mongo.db.requests.update_one({'_id': self.request1_oid},
                                                      {'$push': {'comments': {'$each': cmts}}})

        with app.test_client() as c:
            res = c.get('/requests/' + self.request1_id + '/comments')
            assert res.status_code == 200

            assert res.data is not None
            d_json = ujson.loads(res.data)
            assert isinstance(d_json, list)
            assert len(d_json) == len(self.comments)

    def test_post_request_comments(self):
        cmt = {
            'posted_by': self.volunteer_id,
            'text': self.comments[0]
        }
        with app.test_client() as c:
            res = c.post('/requests/' + self.request1_id + '/comments', content_type="application/json",
                         data=ujson.dumps(cmt))
            assert res.status_code == 201

            r = mongo.db.requests.find_one_or_404({'_id': self.request1_oid})
            cmts = r.get('comments')
            assert cmts is not None
            assert len(cmts) == 1
            cmt = cmts[0]
            assert 'mansikkaa' in cmt['text']
            assert cmt['posted_on'].date().isoformat() == self.today_iso
            assert cmt['_id'] is not None

    def test_get_request_comment(self):
        cmt = {
            'posted_by': self.volunteer_id,
            'text': self.comments[0]
        }
        with app.test_client() as c:
            res = c.post('/requests/' + self.request1_id + '/comments', content_type="application/json",
                         data=ujson.dumps(cmt))
            assert res.status_code == 201

            r = mongo.db.requests.find_one_or_404({'_id': self.request1_oid})
            cmts = r.get('comments')
            assert cmts is not None
            assert len(cmts) == 1
            cmt = cmts[0]

            res = c.get('/requests/' + self.request1_id + '/comments/' + str(cmt['_id']))
            assert res.status_code == 200
            assert res.data is not None
            d_json = ujson.loads(res.data)
            assert d_json['_id'] == str(cmt['_id'])
            assert d_json['posted_on'].startswith(self.today_iso)


    def test_delete_request_comment(self):
        cmt = {
            'posted_by': self.volunteer_id,
            'text': self.comments[0]
        }
        with app.test_client() as c:
            res = c.post('/requests/' + self.request1_id + '/comments', content_type="application/json",
                         data=ujson.dumps(cmt))
            assert res.status_code == 201

            r = mongo.db.requests.find_one_or_404({'_id': self.request1_oid})
            cmts = r.get('comments')
            assert cmts is not None
            assert len(cmts) == 1
            cmt = cmts[0]

            res = c.delete('/requests/' + self.request1_id + '/comments/' + str(cmt['_id']))
            assert res.status_code == 204

            r = mongo.db.requests.find_one_or_404({'_id': self.request1_oid})
            cmts = r.get('comments')
            assert cmts is not None
            assert len(cmts) == 0
